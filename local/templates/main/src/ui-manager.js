$(function() {
    const
        uiRanges = $('.ui-range'),
        uiSelect = $('.ui-select'),
        uiFormSelect = $('.selectpicker'),
        uiInputs = $('.ui-input'),
        uiTextareas = $('.ui-textarea'),

        cardNumberInputs = $('input[data-valtype="card-number"]'),
        cardDateInputs = $('input[data-valtype="card-date"]'),
        cardCodeInputs = $('input[data-valtype="card-code"]'),
        phoneInputs = $('input[data-valtype="tel"]'),

        dateInputs = $('.ui-input.date'),

        transactionForm = $('#transaction-form'),
        practicesignupForm = $('#practice-signup'),
        interviewsignupForm = $('#interview-signup');

    if (uiRanges.length) {
        uiRanges.each(function () {
            let curr = $(this),
                slider = curr.find('.ui-range-slider'),
                handle = null;
            slider.slider({
                range: 'min',
                animate: 200,
                value: 0,
                create: function (e, ui) {
                    handle = curr.find('.ui-slider-handle'); //ниже - фикс выползания за границы
                    handle.css('margin-left', -1 * handle.width() * (slider.slider('value') / slider.slider('option', 'max')));
                },
                change: function (e, ui) {
                    handle.css('margin-left', -1 * handle.width() * (slider.slider('value') / slider.slider('option', 'max')));
                },
                slide: function () {
                    handle.css('margin-left', -1 * handle.width() * (slider.slider('value') / slider.slider('option', 'max')));
                }
            });
        });
    }

    if (uiInputs.length) {
        uiInputs.each(function () {
            let curr = $(this),
                currInput = curr.find('input');
            currInput.on('input change focus', function (e) {
                e.target.value ?
                    curr.addClass('active') :
                    curr.removeClass('active')
            })
        })
    }

    if (uiTextareas.length) {
        uiTextareas.each(function () {
            let curr = $(this),
                currTextarea = curr.find('textarea');
            currTextarea.on('input change focus', function (e) {
                e.target.value ?
                    curr.addClass('active') :
                    curr.removeClass('active')
            })
        })
    }

    if (uiSelect.length) {
        $(uiSelect).selectmenu();
    }

    if (uiFormSelect.length) {
        uiFormSelect.selectpicker();
    }

    if (cardNumberInputs.length) {
        cardNumberInputs.mask('0000 0000 0000 0000');
    }

    if (cardDateInputs.length) {
        cardDateInputs.mask('00/00');
    }

    if (cardCodeInputs.length) {
        cardCodeInputs.mask('000');
    }

    if (phoneInputs.length) {
        phoneInputs.mask('8 000 000 00 00');
    }

    if (transactionForm.length) {
        transactionForm.parsley();
    }

    if (dateInputs.length) {
        dateInputs.each(function () {
            let curr = $(this),
                currInput = curr.find('input');
            currInput.mask('00.00.0000');
            currInput.datepicker({
                dateFormat: "dd.mm.yy"
            })
            //parsley слушает input-event, искусственно вызываем
            currInput.on('change', function(){currInput.trigger('input')})
        })
    }

    if (practicesignupForm.length) {
        practicesignupForm.parsley({
            classHandler: function (el) {
                return el.$element.closest('label');
            },
        });
    }

    if(interviewsignupForm.length) {
        interviewsignupForm.parsley({
            classHandler: function (el) {
                return el.$element.closest('label');
            },
        });
    }
});
$(function() {
    const
        uiPointers = $('.ui-pointer'),
        uiPillBtns = $('.ui-pill-btn'),
        headingToggles = $('.heading-toggle'),
        questionTabToggle = $('.questions-tabs-list-item-toggle'),
        conditionSubToggle = $('.conditions-sub-heading a');

    questionTabToggle.on('click', function () {
        let curr = $(this),
            currDrop = curr.siblings('.questions-tabs-list-item-drop');
        curr.toggleClass('active');
        currDrop.slideToggle(300);
        let tabsSlider = $('.tabs-content-list');
        setTimeout(function() {
            tabsSlider.slick("setOption", '', '', true);
        }, 500);
    });

    uiPointers.each(function () {
        let curr = $(this),
            currItems = curr.find('.ui-pointer-list-item');
        $(currItems[0]).addClass('active');
        currItems.on('click', function () {
            let currItem = $(this);
            currItems.removeClass('active');
            currItem.addClass('active');
        });
    });

    headingToggles.on('click', function () {
        $(this).toggleClass('toggled');
    });

    uiPillBtns.each(function () {
        let curr = $(this),
            currBtns = curr.find('button');
        currBtns.on('click', function () {
            let currBtn = $(this);
            currBtns.removeClass('active');
            currBtn.addClass('active');
        });
    });

    conditionSubToggle.on('click', function (e) {
        e.preventDefault();
        let drop = $(this).closest('.conditions-sub-heading').siblings('.conditions-sub-drop');
        drop.slideToggle(500)
        let tabsSlider = $('.tabs-content-list');
        setTimeout(function() {
            tabsSlider.slick("setOption", '', '', true);
        }, 500);
    })

    $('.popup-top-close').on('click', function () {
        $.fancybox.close();
        $(window).keydown(function (e) {
            if (e.keyCode == 27) {
                $.fancybox.close();
            }
        })
    })

    $('.top-helpers-search-toggle').on('click', function () {
        let curr = $('.top-helpers-search-toggle');
        let input = $('.top-helpers-search input');

        setTimeout(function () {
            input.focus();
        }, 200);

        curr.toggleClass('active');

        $(window).keydown(function (e) {
            if (e.keyCode == 27) {
                curr.removeClass('active');
            }
        })
    })

    function burger() {
        $('.top-helpers-mb-button').on('click', function () {
            $('html').css('overflow', 'auto');
            $(this).toggleClass('active');
            $('.top-helpers-mb-wrapper').toggleClass('active');
            if ($('.top-helpers-mb-wrapper').hasClass('active')) {
                $('html').css('overflow', 'hidden');
            }
        });
    }
    burger();
});

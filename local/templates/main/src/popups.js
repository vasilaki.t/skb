$(function () {
    const
        interviewSignup = $('.pp-int-sn'),
        vacanciesSuccess = $('.pp-vc-scs');

    interviewSignup.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "popups/interview-signup.html",
            data: ({}),
            success: function (a) {
                $.fancybox.close(true);
                $.fancybox.open(a, {
                    infobar: false,
                    toolbar: false,
                    smallBtn: false,
                    parentEl: '.wrapper',
                    afterLoad: function () {
                        let uiInputs = $(this)[0].$content.find('.ui-input');
                        uiInputs.each(function () {
                            let curr = $(this),
                                currInput = curr.find('input');
                            currInput.on('input change focus', function (e) {
                                e.target.value ?
                                    curr.addClass('active') :
                                    curr.removeClass('active')
                            })
                        })
                        $(this)[0].$content.find('input[data-valtype="tel"]').mask('8 000 000 00 00');
                        $(this)[0].$content.find('.selectpicker').selectpicker();
                        let selectAction = $(this)[0].$content.find('.ui-form-select-action button');
                        selectAction.on('click', function (e) {
                            e.preventDefault() //отключение parsley-валидации при клике
                            console.log('geo selection')
                        })
                        $(this)[0].$content.find('#pp-interview-signup').parsley({
                            classHandler: function (el) {
                                return el.$element.closest('label');
                            },
                        });
                    }
                });

            }
        });
    });

    vacanciesSuccess.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "popups/vacancies-success.html",
            data: ({}),
            success: function (a) {
                $.fancybox.close(true);
                $.fancybox.open(a, {
                    infobar: false,
                    toolbar: false,
                    smallBtn: false,
                    parentEl: '.wrapper',
                    afterLoad: function () {
                        $(this)[0].$content.find('.ui-btn a').on('click', function(e) {
                            e.preventDefault();
                            $.fancybox.close();
                        })
                    }
                });

            }
        });
    });
});
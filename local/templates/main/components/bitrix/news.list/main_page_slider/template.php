<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (!empty($arResult['ITEMS'])): ?>
    <section class="promo">
        <div class="promo-slider">
            <div class="promo-slider-block">
                <? foreach ($arResult['ITEMS'] as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div class="promo-slider-block-item <?= $arItem['CODE'] ?>"
                         id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="promo-slider-block-item-content">
                            <div class="promo-slider-block-item-content-title"><?= $arItem['NAME'] ?></div>
                            <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
                                <div class="promo-slider-block-item-content-desc"><?= $arItem['PREVIEW_TEXT'] ?></div>
                            <? endif; ?>
                            <? if (!empty($arItem['DETAIL_TEXT'])): ?>
                                <div class="ui-btn">
                                    <a href="<?= $arItem['PROPERTIES']['pageUrl']['VALUE'] ?>"><?= $arItem['DETAIL_TEXT'] ?></a>
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="promo-slider-block-item-img">
                            <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="error">
                            <svg width="739" height="430" viewBox="0 0 739 430" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M340 -55H1300V445H0L340 -55Z" fill="url(#slide<?= $arItem['ID'] ?>_linear)"/>
                                <defs>
                                    <linearGradient id="slide<?= $arItem['ID'] ?>_linear" x1="1300" y1="445.001"
                                                    x2="3.58218e-07" y2="445.001" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#6CC47B"/>
                                        <stop offset="1" stop-color="#038C73"/>
                                    </linearGradient>
                                </defs>
                            </svg>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="slick-controls">
            <div class="slick-controls-arrows"></div>
            <div class="slick-controls-dots"></div>
        </div>
    </section>
<? endif; ?>
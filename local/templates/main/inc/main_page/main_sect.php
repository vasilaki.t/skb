<main>
    <div class="m-inner">
        <div class="offers">
            <div class="offers-slider">
                <div class="offers-slider-block">
                    <div class="offers-slider-block-item">
                        <div class="offers-slider-block-item-img">
                            <img src="media/img/offers_slide_img1.png" alt="error">
                        </div>
                        <div class="offers-slider-block-item-title">
                            Карта с <span>кэшбэком</span> 1
                        </div>
                        <div class="offers-slider-block-item-txt">
                            Карта для выгодных покупок
                        </div>
                        <div class="offers-slider-block-item-list">
                            <div class="offers-slider-block-item-list-item">
                                <div class="offers-slider-block-item-list-item-prop">
                                    <span>до</span>
                                    300 000 ₽
                                </div>
                                <div class="offers-slider-block-item-list-item-val">
                                    кредитный лимит
                                </div>
                            </div>
                            <div class="offers-slider-block-item-list-item">
                                <div class="offers-slider-block-item-list-item-prop">
                                    62 дня
                                </div>
                                <div class="offers-slider-block-item-list-item-val">
                                    безпpоцентный пеpиод
                                </div>
                            </div>
                        </div>
                        <div class="ui-btn-hollow">
                            <a href="#">Получить деньги</a>
                        </div>
                    </div>
                    <div class="offers-slider-block-item">
                        <div class="offers-slider-block-item-img">
                            <img src="media/img/offers_slide_img1.png" alt="error">
                        </div>
                        <div class="offers-slider-block-item-title">
                            Карта с <span>кэшбэком</span> 2
                        </div>
                        <div class="offers-slider-block-item-txt">
                            Карта для выгодных покупок
                        </div>
                        <div class="offers-slider-block-item-list">
                            <div class="offers-slider-block-item-list-item">
                                <div class="offers-slider-block-item-list-item-prop">
                                    <span>до</span>
                                    300 000 ₽
                                </div>
                                <div class="offers-slider-block-item-list-item-val">
                                    кредитный лимит
                                </div>
                            </div>
                            <div class="offers-slider-block-item-list-item">
                                <div class="offers-slider-block-item-list-item-prop">
                                    62 дня
                                </div>
                                <div class="offers-slider-block-item-list-item-val">
                                    безпpоцентный пеpиод
                                </div>
                            </div>
                        </div>
                        <div class="ui-btn-hollow">
                            <a href="#">Получить деньги</a>
                        </div>
                    </div>
                    <div class="offers-slider-block-item">
                        <div class="offers-slider-block-item-img">
                            <img src="media/img/offers_slide_img1.png" alt="error">
                        </div>
                        <div class="offers-slider-block-item-title">
                            Карта с <span>кэшбэком</span> 3
                        </div>
                        <div class="offers-slider-block-item-txt">
                            Карта для выгодных покупок
                        </div>
                        <div class="offers-slider-block-item-list">
                            <div class="offers-slider-block-item-list-item">
                                <div class="offers-slider-block-item-list-item-prop">
                                    <span>до</span>
                                    300 000 ₽
                                </div>
                                <div class="offers-slider-block-item-list-item-val">
                                    кредитный лимит
                                </div>
                            </div>
                            <div class="offers-slider-block-item-list-item">
                                <div class="offers-slider-block-item-list-item-prop">
                                    62 дня
                                </div>
                                <div class="offers-slider-block-item-list-item-val">
                                    безпpоцентный пеpиод
                                </div>
                            </div>
                        </div>
                        <div class="ui-btn-hollow">
                            <a href="#">Получить деньги</a>
                        </div>
                    </div>
                </div>
                <div class="slick-controls">
                    <div class="slick-controls-counter">
                        <span class="slick-controls-counter-current">0</span>
                        <span>/</span>
                        <span class="slick-controls-counter-total">0</span>
                    </div>
                    <div class="slick-controls-arrows"></div>
                </div>
            </div>
            <div class="offers-static">
                <div class="offers-static-img">
                    <img src="media/img/ipoteka.png" alt="error">
                </div>
                <div class="desktop-content">
                    <div class="offers-static-title">
                        Ипотека
                    </div>
                    <div class="offers-static-txt">
                        Мечтаете о&nbsp;квартире в&nbsp;новостройке?
                        Сделайте первые шаги&nbsp;&mdash; рассчитайте кредит и&nbsp;оформите заявку
                    </div>
                </div>
                <div class="mobile-content">
                    <div class="offers-static-title">
                        Мечтаете о квартире
                        в&nbsp;новостройке?
                    </div>
                    <div class="offers-static-txt">
                        Сделайте первые шаги
                    </div>
                </div>
                <div class="ui-btn-hollow">
                    <a href="#">Узнать больше</a>
                </div>
            </div>
        </div>
        <div class="credit">
            <div class="credit-title">
                <h2>Кpедит на любые цели</h2>
            </div>
            <div class="credit-body">
                <div class="credit-body-calc">
                    <div class="ui-range">
                        <div class="ui-range-titles">
                            <div class="ui-range-titles-left">
                                Сумма кpедита
                            </div>
                            <div class="ui-range-titles-right">
                                700 000 ₽
                            </div>
                        </div>
                        <div class="ui-range-slider"></div>
                        <div class="ui-range-ranges">
                            <div class="ui-range-ranges-range">60 000 ₽</div>
                            <div class="ui-range-ranges-range">1 000 000 ₽</div>
                        </div>
                    </div>
                    <div class="ui-pointer">
                        <div class="ui-pointer-title">
                            Сpок кpедита, месяцев
                        </div>
                        <div class="ui-pointer-list">
                            <div class="ui-pointer-list-item">
                                <div class="ui-pointer-list-item-num">12</div>
                                <div class="ui-pointer-list-item-indicator"></div>
                                <div class="ui-pointer-list-item-desc">1 год</div>
                            </div>
                            <div class="ui-pointer-list-item">
                                <div class="ui-pointer-list-item-num">24</div>
                                <div class="ui-pointer-list-item-indicator"></div>
                                <div class="ui-pointer-list-item-desc">2 года</div>
                            </div>
                            <div class="ui-pointer-list-item">
                                <div class="ui-pointer-list-item-num">36</div>
                                <div class="ui-pointer-list-item-indicator"></div>
                                <div class="ui-pointer-list-item-desc">3 года</div>
                            </div>
                            <div class="ui-pointer-list-item">
                                <div class="ui-pointer-list-item-num">48</div>
                                <div class="ui-pointer-list-item-indicator"></div>
                                <div class="ui-pointer-list-item-desc">4 года</div>
                            </div>
                            <div class="ui-pointer-list-item">
                                <div class="ui-pointer-list-item-num">60</div>
                                <div class="ui-pointer-list-item-indicator"></div>
                                <div class="ui-pointer-list-item-desc">5 лет</div>
                            </div>
                        </div>
                    </div>
                    <div class="ui-checkbox">
                        <label>
                            <input type="checkbox" name="filter1" id="filter1">
                            <span class="checkmark"></span>
                            Спpавка о доходах
                            <div class="ui-tip">
                                <div class="ui-tip-ico">

                                </div>
                                <div class="ui-tip-drop">
                                    Без подтверждения дохода максимальная сумма кредита&nbsp;&mdash;
                                    200&nbsp;000&nbsp;₽
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="credit-body-result">
                    <div class="credit-body-result-content">
                        <div class="credit-body-result-content-block">
                            <div class="credit-body-result-content-block-item">
                                <div class="credit-body-result-content-block-item-val">
                                    64 305 ₽
                                </div>
                                <div class="credit-body-result-content-block-item-prop">
                                    Ежемесячный платеж
                                </div>
                            </div>
                            <div class="credit-body-result-content-block-item">
                                <div class="credit-body-result-content-block-item-val">
                                    9.9%
                                </div>
                                <div class="credit-body-result-content-block-item-prop">
                                    Ставка
                                </div>
                            </div>
                        </div>
                        <div class="credit-body-result-content-action">
                            <div class="credit-body-result-content-action-img">
                                <img src="media/img/couple.png" alt="error">
                            </div>
                            <div class="ui-btn">
                                <a href="#">Получить кредит</a>
                            </div>
                        </div>
                    </div>
                    <div class="credit-body-result-info">
                        Пpедваpительный pасчет. Не является публичной офеpтой.
                    </div>
                </div>
            </div>
        </div>
        <div class="share">
            <div class="share-slider">
                <div class="share-slider-block">
                    <div class="share-slider-block-item">
                        <div class="share-slider-block-item-img">
                            <img src="media/img/share_slide_img1.png" alt="error">
                        </div>
                        <div class="share-slider-block-item-content">
                            <div class="tags sm">
                                <div class="tags-block">
                                    <div class="tags-block-item">
                                        <a href="#">Акция</a>
                                    </div>
                                </div>
                            </div>
                            <div class="share-slider-block-item-content-txt">
                                <div class="desktop-content">
                                    <div class="share-slider-block-item-content-txt-title">
                                        <h2>Вклад «Обыкновенное чудо»</h2>
                                    </div>
                                    <div class="share-slider-block-item-content-txt-desc">
                                        Вклад с&nbsp;максимальной доходностью на&nbsp;длительный срок. Позволит
                                        сохранить и&nbsp;приумножить свои сбережения.
                                    </div>
                                </div>
                                <div class="mobile-content">
                                    <h2>«Обыкновенное чудо»</h2>
                                    <div class="share-slider-block-item-content-txt-desc">
                                        Вклад с максимальной
                                        доходностью
                                        на&nbsp;длительный срок
                                    </div>
                                </div>
                                <div class="ui-btn-hollow">
                                    <a href="#">Узнать больше</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="share-slider-block-item">
                        <div class="share-slider-block-item-img">
                            <img src="media/img/share_slide_img1.png" alt="error">
                        </div>
                        <div class="share-slider-block-item-content">
                            <div class="tags sm">
                                <div class="tags-block">
                                    <div class="tags-block-item">
                                        <a href="#">Акция</a>
                                    </div>
                                </div>
                            </div>
                            <div class="share-slider-block-item-content-txt">
                                <div class="desktop-content">
                                    <div class="share-slider-block-item-content-txt-title">
                                        <h2>Вклад «Обыкновенное чудо»</h2>
                                    </div>
                                    <div class="share-slider-block-item-content-txt-desc">
                                        Вклад с&nbsp;максимальной доходностью на&nbsp;длительный срок. Позволит
                                        сохранить и&nbsp;приумножить свои сбережения.
                                    </div>
                                </div>
                                <div class="mobile-content">
                                    <h2>«Обыкновенное чудо»</h2>
                                    <div class="share-slider-block-item-content-txt-desc">
                                        Вклад с максимальной
                                        доходностью
                                        на&nbsp;длительный срок
                                    </div>
                                </div>
                                <div class="ui-btn-hollow">
                                    <a href="#">Узнать больше</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick-controls">
                    <div class="slick-controls-dots"></div>
                </div>
            </div>
        </div>
        <div class="news">
            <div class="news-title">
                <div class="news-title-heading">
                    <h2>Новости</h2>
                </div>
                <div class="ui-arrow-link">
                    <a href="#">Смотреть все</a>
                </div>
            </div>
            <div class="news-block">
                <div class="news-block-item">
                    <a href="#">
                        <div class="news-block-item-date">
                            20 сентябpя
                        </div>
                        <div class="news-block-item-txt">
                            Рейтинг мобильных банковских приложений MarksWebb: ДелоБанк – лидер во всех
                            номинациях
                        </div>
                    </a>
                </div>
                <div class="news-block-item">
                    <a href="#">
                        <div class="news-block-item-date">
                            28 августа
                        </div>
                        <div class="news-block-item-txt">
                            Проведена первая в России оплата покупки по QR-коду через Систему быстрых платежей
                        </div>
                    </a>
                </div>
                <div class="news-block-item">
                    <a href="#">
                        <div class="news-block-item-date">
                            23 августа
                        </div>
                        <div class="news-block-item-txt">
                            СКБ-банк внедрил технологии Smart Engines для распознавания паспортов и банковских
                            карт
                        </div>
                    </a>
                </div>
                <div class="news-block-item">
                    <a href="#">
                        <div class="news-block-item-date">
                            15 августа
                        </div>
                        <div class="news-block-item-txt">
                            Бонус к празднику: безлимитные платежи и бесплатные консультации юриста от ДелоБанка
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="banners">
            <div class="banners-promo">
                <div class="banners-promo-img">
                    <img src="media/img/banner_promo.png" alt="error" class="banners-promo-img-main">
                    <div class="banners-promo-img-green">
                        <svg width="530" height="320" viewBox="0 0 530 320" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M206 0H530L324 320H0L206 0Z" fill="url(#paint0_linear)"/>
                            <defs>
                                <linearGradient id="paint0_linear" x1="530" y1="320.001" x2="1.46043e-07"
                                                y2="320.001" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#6CC47B"/>
                                    <stop offset="1" stop-color="#038C73"/>
                                </linearGradient>
                            </defs>
                        </svg>
                    </div>
                    <div class="banners-promo-img-tip left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             width="32" height="32" viewBox="0 0 32 32" fill="none">
                            <rect width="32" height="32" fill="url(#pattern0)"/>
                            <defs>
                                <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1"
                                         height="1">
                                    <use xlink:href="#image0" transform="scale(0.0178571)"/>
                                </pattern>
                                <image id="image0" width="56" height="56"
                                       xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAYAAACohjseAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkY2QjQwMDFGMDEyMTExRUE4RjFGOEYzRUIyQzY0Q0U3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkY2QjQwMDIwMDEyMTExRUE4RjFGOEYzRUIyQzY0Q0U3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjZCNDAwMUQwMTIxMTFFQThGMUY4RjNFQjJDNjRDRTciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjZCNDAwMUUwMTIxMTFFQThGMUY4RjNFQjJDNjRDRTciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5bsfgjAAAHG0lEQVR42txazW8bRRR/M7sbOyXC7oVrLJR7zR+AmtyRSE8IpKKkVFyQWjtpS29NL4BoPhoEN9S4EhdOafkHaiFxJr1byubKgTpSadPEu483H2vv2vu9myrNSLbXM7Oz72ve/N57y+CHmw2wjB1gcBmQvgERgNEvNUY9ss9rNMbYAfW1YXXzCfjbZvseIFsbzvPWCG9dun9hordzowFQ2QWOTbDoKQatYXi/qH45zTOpj8sxG7jbho8fPEHxyJDGwTJ3JXNDpphiTNLpY04O0xfiLPU/goc364GVXINNMCfW8a83XAsaodS4lWf03RzOGj1Y//q4UGs3gPOdue439ShJmnRTUz6eO/NwjAeQ1KYMIoKYPJKEdIf9t9bXyBo68toyEE6cSQ1O8XnS8g59wsWN2JB8ue4CvBnYMF2VFCrmxYfGKnTrgK4HNHbhzT7dU+/VPqhRTz+KQSVxl9twdzOZwc0VJU3Tmhy7u23H3rvVIoKYNI3YVqnuw+ffJdPy57dawa8ip/CEvRLRhFBOst/mmJ7pRj+TYfZ14UIcgzmalIkF+e6NUd/YNktey5sbrUEzF5GI+ZgTHtGNYxHjBRB2Q4L9ZdegixBr1vdb9cQ1eKJ5pGxc+lJ4nfSoLDLjZvQe3GxvwQx/Aeu35sP3YIK8GctGDHPV5OmkPZjJzTiakpA9iEydYeg0cpt+Jp+nJ79+lcggluYolOXk3KQ5HV6iBjGbDtX8k+weyNCCcUs6IoaCjveiAgHUafIeHeJ90GYt9m8E/bOSRRPszE5icHII3IhzMooWPN6D32/3SSAkFKaoNLQ6uKZa9CvsDHOHLw/jGGwJmEvz6opRBkHH498Y+hrxPrRiUIuL4UxaJBQHHtP4XoRG2vQtgH9NC33kH6RhsBFWHvXf783/0gf8OWJJz7W/ZzZTmcV/gz249zAU98HGqsCpwoMuUbTwONee6tyoUwDQBNMcqUBqDPW1+CIwWgGcc149l8xJPjHmoFcEd4vveIzXYJq29FNqWnppTrWSXRqctcZPZbW3dEyIODAuFsyPRWNhXEETzdB61szftC1qxOSH0fHg+oqIoueVdXkeMw2kiJkTpcHN9iW6pys9pHL/SFpn8lf8N0ClJKb0fy6PA5Ga2INPfvwoZEUZIPemZiIDXrE8Mee5oDCCxZjIxfjRSwRz3hyHRZ1LgshaYC0vFeJtYD52pip5xwP46UQTFWebs0De1y5sNyJdcWfjIBYFcdIiDpal65dM0cWxw6A6rQCSiQryWp58j/rxqIml2IPM2E+VsijUTAXWBV3t7eLCTIHMef7oNU8bBKyxnBZPf86cTAENSnrKlmnZOZkzhQfiFcQnPeQpNhdt8tg2uOxpSebZoY89d/jPYbQsN1ZweKasjjkZAcJneE1ei0Supd2el9T1J3j9Y+J69bQd1vhOxBxIZkZmsZuaAf9REH7t//+gvQy3tzpnBItGsM8obpPmq8cD5j5WJwjKsw/MteFMtPV2C9ZvrcE73oSJhn0YnJMWXT475+3cM1hePKiOlB06656eBe85YlCUsE1DVHmbPnTOIkMjRhEHwtpEUul98xK47iJNbcosXa6E02oTqmyXYsCGjhEp8tCl61HMSHZHAbUhMQqBBlXCjjZRy2NuDPoMK8djCVIkQICwNVHCLsPop4g5UZZmIVAMvWThMEYUwp6l0OtRUgn7ki4bz4OTUMIWCIXJjFcDBlZzIvtVHPI1dMC8IHOoqKIr+RGla5ESMaqqhF0V/cf7pICLvdpMXAlbS4hzG26nLWFLTBbsd2QgyyL9dToQrg4ui2LTT79PpuWvOzhMCJfrRcWqYdUlFwtFX5gnWEyMJnKGKDjAPBF2/LIZg0VVT8TkAmguYsyIpFORlGFGDfpfyZG+Z/SBxWt1pt8bytnG9qAxUPvBPd0URFAeooSNE1k1/OLrJtSMF/jl9TVeWrDrvSJSFBsxxgrLwhjoSjNc5lA24C6qQcxl1iXvwTCty+ptUSeTx08JNZaddAozDQdKMFGZ5c2uwddpSthAyGFjJeie5OuUY+/FoB6LKmELKCfzPD5cK9dhz2lvLEBrux9Dqxi7COjY8McdlPvRdPXrlEyVrQ1XvUrpa3PHLw97kRpEfkWC1jCzHjLnV5lEK2sTJWw86QdNl7EAjmTYSKHANlHUHxos857PRhuUTZhQ26vyRlk9SOB8BPVEHCoyZm9IylEl7Lh14u4LRBQ36nCxGlyjovmpVhUG9TR39G8/jDm8er1FnG2RPLoM32bm/pRPjRGDX+3QjUt0tX3uGMTPlhpQMff1kbVwrlIWijnrmQbuT9hvv3bNd4Lwq9eWEqbQnjUuk1NaVMcMOU1j0Nb4/WybKFy9vkjHwm4GJNQl5pZZp2OXm3Q6tayRs0c4t5uwU0Um/QBcZZb+kf8FGAAOncN3StONhwAAAABJRU5ErkJggg=="/>
                            </defs>
                        </svg>
                        <span>Оплачивай квитанции в один клик по QR-коду</span>
                    </div>
                    <div class="banners-promo-img-tip right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none">
                            <path
                                d="M12 2C6.486 2 2 6.486 2 12C2 17.514 6.486 22 12 22C17.514 22 22 17.514 22 12C22 6.486 17.514 2 12 2ZM10.001 16.413L6.288 12.708L7.7 11.292L9.999 13.587L15.293 8.293L16.707 9.707L10.001 16.413Z"
                                fill="#038C73"/>
                        </svg>
                        <span>Пользуйся 24 часа в сутки</span>
                    </div>
                </div>
                <div class="banners-promo-content">
                    <div class="banners-promo-content-title">
                        <h2>СКБ-Online</h2>
                    </div>
                    <div class="banners-promo-content-txt">
                        <div class="desktop-content">
                            <ul class="list">
                                <li>
                                    СКБ-online&nbsp;&mdash; это удобный и&nbsp;современный интернет-банк
                                    и&nbsp;мобильное приложение.
                                </li>
                                <li>
                                    Подключение и&nbsp;использование СКБ-online бесплатно.
                                </li>
                            </ul>
                        </div>

                        <div class="mobile-content">
                            <ul class="list">
                                <li>
                                    Оплачивай квитанции
                                    в&nbsp;один клик по QR-коду
                                </li>
                                <li>
                                    Пользуйся 24 часа в&nbsp;сутки
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="ui-app-list">
                        <div class="ui-app-list-item">
                            <a href="#">
                                <img class="default" src="media/img/big_app_store.svg" alt="error">
                                <img class="hover" src="media/img/big_app_store_hover.svg" alt="error">
                            </a>
                        </div>
                        <div class="ui-app-list-item">
                            <a href="#">
                                <img class="default" src="media/img/big_google_play.svg" alt="error">
                                <img class="hover" src="media/img/big_google_play_hover.svg" alt="error">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banners-list">
                <div class="banners-list-item">
                    <div class="banners-list-item-img">
                        <img src="media/img/card-to-card.png" alt="error">
                    </div>
                    <div class="banners-list-item-content">
                        <div class="banners-list-item-content-title">
                            Пеpеводы с каpты на каpту
                        </div>
                        <div class="banners-list-item-content-txt">
                            Мгновенные переводы денег между картами любых банков
                        </div>
                        <div class="ui-btn-hollow">
                            <a href="#">Перевести деньги</a>
                        </div>
                    </div>
                </div>
                <div class="banners-list-item atms">
                    <div class="banners-list-item-img">
                        <img src="media/img/terminals.png" alt="error">
                    </div>
                    <div class="banners-list-item-content">
                        <div class="banners-list-item-content-title">
                            Офисы и банкоматы
                        </div>
                        <div class="banners-list-item-content-txt">
                            Найдите ближайшее отделение
                        </div>
                        <div class="ui-btn-hollow">
                            <a href="#">Перевести деньги</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="help">
            <div class="help-title">
                <h2>Чем еще помочь?</h2>
            </div>
            <div class="tags lg">
                <div class="tags-block">
                    <div class="tags-block-item">
                        <a href="#">Рассчитать ипотеку</a>
                    </div>
                    <div class="tags-block-item active">
                        <a href="#">Открыть вклад</a>
                    </div>
                    <div class="tags-block-item">
                        <a href="#">Страхование</a>
                    </div>
                    <div class="tags-block-item">
                        <a href="#">Акции</a>
                    </div>
                    <div class="tags-block-item">
                        <a href="#">Переводы</a>
                    </div>
                    <div class="tags-block-item">
                        <a href="#">Сейфовые ячейки</a>
                    </div>
                    <div class="tags-block-item">
                        <a href="#">Экспресс кредит</a>
                    </div>
                    <div class="tags-block-item">
                        <a href="#">Карта с преимуществами</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1587408253_main_page_slider extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Scenario title
     **/
    public static function name() {
        return 'main_page_slider';
    }

    /**
     * Priority of scenario
     **/
    public static function priority() {
        return self::PRIORITY_MEDIUM;
    }

    /**
     * @return string hash
     */
    public static function hash() {
        return '1b8967ede1cac1c38a2055f22dbe4dada89a637c';
    }

    /**
     * @return int approximately time in seconds
     */
    public static function approximatelyTime() {
        return 0;
    }

    /**
     * Writes action by apply scenario. Use method `setData` to save needed rollback data.
     * For printing info into console use object from $this->printer() method.
     **/
    public function commit() {
        $ibBuilder = new \WS\ReduceMigrations\Builder\IblockBuilder();
        $ibBuilder->createIblock('content', 'Слайдер для главной', function (\WS\ReduceMigrations\Builder\Entity\Iblock $iblock) {
            $iblock
                ->siteId('s1')
                ->sort(500)
                ->code('main_page_slider')
                ->groupId(array(
                    '2' => 'R'
                ));
            $iblock
                ->addProperty('Ссылка на страницу')
                ->code('pageUrl');

        });
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data.
     * For printing info into console use object from $this->printer() method.
     **/
    public function rollback() {
        // my code
    }
}

<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1587408340_dobavlenie_slaydera_dlya_glavnoy extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Scenario title
     **/
    public static function name() {
        return 'Добавление слайдера для главной';
    }

    /**
     * Priority of scenario
     **/
    public static function priority() {
        return self::PRIORITY_MEDIUM;
    }

    /**
     * @return string hash
     */
    public static function hash() {
        return '71342501eec6ba294f6a9c2e5ccea0282fe76945';
    }

    /**
     * @return int approximately time in seconds
     */
    public static function approximatelyTime() {
        return 0;
    }

    /**
     * Writes action by apply scenario. Use method `setData` to save needed rollback data.
     * For printing info into console use object from $this->printer() method.
     **/
    public function commit() {
        // my code
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data.
     * For printing info into console use object from $this->printer() method.
     **/
    public function rollback() {
        // my code
    }
}
